package ua.kh.courses.core.command;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.kh.courses.core.exception.AppException;
import ua.kh.courses.path.Path;

public class ForgetPageCommand extends Command {
	

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 5241608051371710955L;

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		return Path.PAGE_FORGET;
	}

}
