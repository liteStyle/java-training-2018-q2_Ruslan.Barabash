package ua.kh.courses.core.command;

import java.io.IOException;



import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.kh.courses.core.db.dao.DAOFactory;
import ua.kh.courses.core.db.entity.Course;
import ua.kh.courses.core.exception.AppException;
import ua.kh.courses.path.Path;



public class CourseListCommand extends Command{

	/**
	 * 
	 */
	private static final long serialVersionUID = 625793218072200760L;

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		DAOFactory 	mysqlFactory=DAOFactory.getDAOFactory(DAOFactory.MYSQL);
		List <Course> b = new ArrayList<Course>();
	
			b=mysqlFactory.getCourseDAO().findAllCourses();
			System.out.println(b);
			
			
		
	request.setAttribute("course_list", b);
		return Path.ADMIN_CABINET;
	}

}
