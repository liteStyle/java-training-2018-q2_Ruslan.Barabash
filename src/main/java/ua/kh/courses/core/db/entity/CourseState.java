package ua.kh.courses.core.db.entity;


/**
 * Course state entity.
 */
public enum CourseState {

 RECRUITED, DURING, FINISHED;

	public String getName() {
		return name().toLowerCase();
	}

	public static CourseState getCourseState(int requestState) {
		return CourseState.values()[requestState];
	}
	
}
