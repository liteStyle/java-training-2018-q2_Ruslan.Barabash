package ua.kh.courses.core.db.entity;
/**
 * Sorting entity.
 */
public enum SortingType {
NAME_A, NAME_Z, DURATION, COUNT, NONE;
}
