package ua.kh.courses.core.util;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;

import ua.kh.courses.core.db.dao.sql.SQLUserDAO;
import ua.kh.courses.core.db.entity.User;
import ua.kh.courses.core.exception.DBException;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;


/**
 * Utils methods for commands
 */

public class Utils {

    public static final Logger LOG = Logger.getLogger(Utils.class);


    /**
     * Hash the input string in the md5
     * @param pass
     * @return
     */
    public static String encrypt(String pass) {
        StringBuilder hex = new StringBuilder();
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(pass.getBytes("UTF-8"));
            byte[] hash = digest.digest();
            for (int i = 0; i < hash.length; i++) {
                hex.append(String.format("%02x", hash[i]));
            }
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            LOG.error(e.getLocalizedMessage());
        }
        return hex.toString();
    }

    /**
     * Mail recipient sends the specified string
     * @param recipient
     * @param text
     */
    public static void sendMail(String recipient, String text) {
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", 587);
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtps.starttls.enable", "false");
        props.put("mail.smtp.port", 587);
 
        Session session = Session.getDefaultInstance(props,
                new Authenticator() {
                    public PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("best0000it@gmail.com", "Bestit30121988");
                    }
                });
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("best0000it@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(recipient));
            message.setSubject("Courses");
            // set content
            message.setContent(text, "text/html; charset=utf-8");

            Transport.send(message);
            LOG.info("Message was sent to -->" + recipient);

        } catch (MessagingException ex) {
            LOG.error("Message wasn't sent to email -->" + recipient, ex);
        }

    }

    /**
     * Generate a message with new password
     * @param login
     * @return message with new password
     * @throws DBException 
     */
    public static String generateMessage(String login) throws DBException {
        String alphabet = "abcdefghijklmnopqrstuvwxyz1234567890";
        User user = new SQLUserDAO().findUserByLogin(login);
        StringBuilder newPass = new StringBuilder();
        int count = (int) ((Math.random() * 30) + 4);
        for (int i = 0; i < count; i++) {
            newPass.append(alphabet.charAt((int) (Math.random() * alphabet.length())));
        }
        String encryptPass = Utils.encrypt(String.valueOf(newPass));
        new SQLUserDAO().setNewPassword(user.getId(), String.valueOf(encryptPass));
        StringBuffer textMail = new StringBuffer();
        textMail.append("<h4>Hi! Your new login details:<br><br>").append("login: ")
                .append(user.getLogin()).append("<br>password: ").append(newPass)
                .append("<br><br><br><i>Sincerelly yours, Team Best IT!.<i><h4>");
        return textMail.toString();
    }
}
