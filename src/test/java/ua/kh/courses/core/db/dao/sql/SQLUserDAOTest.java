package ua.kh.courses.core.db.dao.sql;

import static org.junit.Assert.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.CommonDataSource;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.naming.java.javaURLContextFactory;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;

import ua.kh.courses.core.db.entity.User;
import ua.kh.courses.core.util.Utils;

public class SQLUserDAOTest {

	User user = new User();
    static Context context;
    static CommonDataSource ds;

    public static Logger LOG = Logger.getLogger(SQLUserDAOTest.class);

    @BeforeClass
    public static void setupBeforeClass() throws Exception {
        MysqlConnectionPoolDataSource ds = new MysqlConnectionPoolDataSource();
        PropertyConfigurator.configure("/home/kurson/SummaryTask4/web/WEB-INF/log4j.properties");
        ds.setURL("jdbc:mysql://localhost:3306/courses");
        ds.setUser("root");
        ds.setPassword("remdigga4237");
        DataSource dataSource = ds;
        System.setProperty(Context.INITIAL_CONTEXT_FACTORY, javaURLContextFactory.class.getName());
        context = new InitialContext();
        Context ctx = context.createSubcontext("java");
        ctx.createSubcontext("comp").createSubcontext("env").createSubcontext("jdbc")
                .bind("courses", dataSource);
        context.bind("java:", ctx);

    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        context.destroySubcontext("java");
        context.unbind("java:");
        context.close();
    }

    @Before
    public void setUp() throws Exception {
        user.setId(1);
        user.setLogin("Admin");
        user.setEmail("ruslan30121988@gmail.com");
        user.setPassword(Utils.encrypt("0000"));
        user.setFname("Ruslan");
        user.setLname("Barabash");
        user.setRoleid(0);
    }


    @Test
    public void findUserByLogin() throws Exception {
        User user = new SQLUserDAO().findUserByLogin("root");
        assertEquals(user.getLogin(), user.getLogin());
    }


    @Test
    public void setNewPassword() throws Exception {
        new SQLUserDAO().setNewPassword(1, Utils.encrypt("pass"));
        assertEquals(Utils.encrypt("pass"), new SQLUserDAO().findUserByLogin("Admin").getPassword());
    }

}
